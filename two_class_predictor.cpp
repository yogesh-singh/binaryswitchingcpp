#include <iostream>
#include <vector>
#include <algorithm>
#include <filesystem>
#include <chrono>
#include <tuple>
#include <numeric>
// #include <armadillo>
#include <iterator>
#include <stdlib.h>
#include <stdio.h>

#include <opencv4/opencv2/imgcodecs.hpp>
#include <opencv4/opencv2/highgui.hpp>
#include <opencv4/opencv2/imgproc.hpp>
#include <opencv4/opencv2/ximgproc.hpp>
#include <opencv2/ximgproc/structured_edge_detection.hpp>
#include <opencv2/core/types.hpp>
#include <opencv2/xfeatures2d.hpp>

namespace fs = std::filesystem;
using namespace std;
using namespace cv;
using namespace cv::ximgproc;
// using namespace arma;

//  https://gist.github.com/Bibimaw/8873663


// #define C1 (float) (0.01 * 255 * 0.01  * 255)
// #define C2 (float) (0.03 * 255 * 0.03  * 255)


	// sigma on block_size
	// double sigma(Mat & m, int i, int j, int block_size)
	// {
	// 	double sd = 0;

	// 	Mat m_tmp = m(Range(i, i + block_size), Range(j, j + block_size));
	// 	Mat m_squared(block_size, block_size, CV_64F);

	// 	multiply(m_tmp, m_tmp, m_squared);

	// 	// E(x)
	// 	double avg = mean(m_tmp)[0];
	// 	// E(x²)
	// 	double avg_2 = mean(m_squared)[0];


	// 	sd = sqrt(avg_2 - avg * avg);

	// 	return sd;
	// }

	// // Covariance
	// double cov(Mat & m1, Mat & m2, int i, int j, int block_size)
	// {
	// 	Mat m3 = Mat::zeros(block_size, block_size, m1.depth());
	// 	Mat m1_tmp = m1(Range(i, i + block_size), Range(j, j + block_size));
	// 	Mat m2_tmp = m2(Range(i, i + block_size), Range(j, j + block_size));


	// 	multiply(m1_tmp, m2_tmp, m3);

	// 	double avg_ro 	= mean(m3)[0]; // E(XY)
	// 	double avg_r 	= mean(m1_tmp)[0]; // E(X)
	// 	double avg_o 	= mean(m2_tmp)[0]; // E(Y)


	// 	double sd_ro = avg_ro - avg_o * avg_r; // E(XY) - E(X)E(Y)

	// 	return sd_ro;
	// }

// double ssim(Mat & img_src, Mat & img_compressed, int block_size, bool show_progress = false)
// 	{
// 		double ssim = 0;

// 		int nbBlockPerHeight 	= img_src.rows / block_size;
// 		int nbBlockPerWidth 	= img_src.cols / block_size;

// 		for (int k = 0; k < nbBlockPerHeight; k++)
// 		{
// 			for (int l = 0; l < nbBlockPerWidth; l++)
// 			{
// 				int m = k * block_size;
// 				int n = l * block_size;

// 				double avg_o 	= mean(img_src(Range(k, k + block_size), Range(l, l + block_size)))[0];
// 				double avg_r 	= mean(img_compressed(Range(k, k + block_size), Range(l, l + block_size)))[0];
// 				double sigma_o 	= sigma(img_src, m, n, block_size);
// 				double sigma_r 	= sigma(img_compressed, m, n, block_size);
// 				double sigma_ro	= cov(img_src, img_compressed, m, n, block_size);

// 				ssim += ((2 * avg_o * avg_r + C1) * (2 * sigma_ro + C2)) / ((avg_o * avg_o + avg_r * avg_r + C1) * (sigma_o * sigma_o + sigma_r * sigma_r + C2));
				
// 			}
// 			// Progress
// 			if (show_progress)
// 				cout << "\r>>SSIM [" << (int) ((( (double)k) / nbBlockPerHeight) * 100) << "%]";
// 		}
// 		ssim /= nbBlockPerHeight * nbBlockPerWidth;

// 		if (show_progress)
// 		{
// 			cout << "\r>>SSIM [100%]" << endl;
// 			cout << "SSIM : " << ssim << endl;
// 		}

// 		return ssim;
// 	}

// https://docs.opencv.org/4.5.2/dd/d3d/tutorial_gpu_basics_similarity.html
// https://searchcode.com/codesearch/view/26663229/

Scalar getMSSIM( const Mat& i1, const Mat& i2)
{
    const double C1 = 6.5025, C2 = 58.5225;
    /***************************** INITS **********************************/
    int d = CV_32FC1;
    Mat I1, I2;
    i1.convertTo(I1, d);            // cannot calculate on one byte large values
    i2.convertTo(I2, d);
    Mat I2_2   = I2.mul(I2);        // I2^2
    Mat I1_2   = I1.mul(I1);        // I1^2
    Mat I1_I2  = I1.mul(I2);        // I1 * I2
    /*************************** END INITS **********************************/
    Mat mu1, mu2;                   // PRELIMINARY COMPUTING
    GaussianBlur(I1, mu1, Size(11, 11), 1.5);
    GaussianBlur(I2, mu2, Size(11, 11), 1.5);
    Mat mu1_2   =   mu1.mul(mu1);
    Mat mu2_2   =   mu2.mul(mu2);
    Mat mu1_mu2 =   mu1.mul(mu2);
    Mat sigma1__2, sigma2__2, sigma_12, sigma1_2, sigma2_2, sigma12;
    GaussianBlur(I1_2, sigma1__2, Size(11, 11), 1.5);
    sigma1_2 = sigma1__2 - mu1_2;
    GaussianBlur(I2_2, sigma2__2, Size(11, 11), 1.5);
    sigma2_2 = sigma2__2 - mu2_2;
    GaussianBlur(I1_I2, sigma_12, Size(11, 11), 1.5);
    sigma12 = sigma_12 - mu1_mu2;
    Mat t1, t2, t3;
    t1 = 2 * mu1_mu2 + C1;
    t2 = 2 * sigma12 + C2;
    t3 = t1.mul(t2);                 // t3 = ((2*mu1_mu2 + C1).*(2*sigma12 + C2))
    t1 = mu1_2 + mu2_2 + C1;
    t2 = sigma1_2 + sigma2_2 + C2;
    t1 = t1.mul(t2);    
    // cout << "t3::" << t3 << endl;
    // cout << "t1::" << t1 << endl;   
    // cout << "sigma12::" << sigma12 << endl;
    // cout << "sigma1_2::" << sigma1_2 << endl;  
    // cout << "sigma2_2::" << sigma2_2 << endl;
    // cout << "mu1_2::" << mu1_2 << endl;   
    // cout << "mu2_2::" << mu2_2 << endl;             // t1 =((mu1_2 + mu2_2 + C1).*(sigma1_2 + sigma2_2 + C2))
    Mat ssim_map;
    divide(t3, t1, ssim_map);        // ssim_map =  t3./t1;
    // cout << "checkrange ssimmap" << checkRange(ssim_map) << "___  " << sum(ssim_map) <<endl;
    Mat mask = (ssim_map != ssim_map);
    int numNaNs = countNonZero(mask);
    // cout << "numNaNs:::: " << numNaNs << endl;
    patchNaNs(ssim_map, 0.0);
    Scalar mssim = cv::mean(ssim_map);   // mssim = average of ssim map
    // cout << "mssim::" << ssim_map << endl; 
    return mssim;
}


// double myssim(const Mat& image_ref, const Mat& image_obj)
// {
// 	double C1 = 6.5025, C2 = 58.5225;
	
// 	// cv::Mat image_ref = cv::imread(ref_image, CV_LOAD_IMAGE_GRAYSCALE);
// 	// cv::Mat image_obj = cv::imread(obj_image, CV_LOAD_IMAGE_GRAYSCALE);
// 	int width = image_ref.cols;
// 	int height = image_ref.rows;
// 	double mean_x = 0;
// 	double mean_y = 0;
// 	double sigma_x = 0;
// 	double sigma_y = 0;
// 	double sigma_xy = 0;
// 	for (int v = 0; v < height; v++)
// 	{
// 		for (int u = 0; u < width; u++)
// 		{
// 			mean_x += image_ref.at<uchar>(v, u);
// 			mean_y += image_obj.at<uchar>(v, u);

// 		}
// 	}
// 	mean_x = mean_x / width / height;
// 	mean_y = mean_y / width / height;
// 	for (int v = 0; v < height; v++)
// 	{
// 		for (int u = 0; u < width; u++)
// 		{
// 			sigma_x += (image_ref.at<uchar>(v, u) - mean_x)* (image_ref.at<uchar>(v, u) - mean_x);
// 			sigma_y += (image_obj.at<uchar>(v, u) - mean_y)* (image_obj.at<uchar>(v, u) - mean_y);
// 			sigma_xy += abs((image_ref.at<uchar>(v, u) - mean_x)* (image_obj.at<uchar>(v, u) - mean_y));
// 		}
// 	}
// 	sigma_x = sigma_x / (width*height - 1);
// 	sigma_y = sigma_y / (width*height - 1);
// 	sigma_xy = sigma_xy / (width*height - 1);
// 	double fenzi = (2 * mean_x*mean_y + C1) * (2 * sigma_xy + C2); 
// 	double fenmu = (mean_x*mean_x + mean_y * mean_y + C1) * (sigma_x + sigma_y + C2);
// 	double ssim = fenzi / fenmu;
// 	return ssim;

// }


// Scalar getMSSIM_gg( cv::Mat img1_temp, cv::Mat img2_temp)
// 	{

// 	double C1 = 6.5025, C2 = 58.5225, C3 = 0.00000681;

// 	cv::Mat kernel = (Mat_<float>(1,11) <<  0.00102818599752740, 0.00759732401586496, 0.03599397767545871, 0.10934004978399577, 0.21296533701490150, 0.26596152026762182, 0.21296533701490150, 0.10934004978399577, 0.03599397767545871, 0.00759732401586496, 0.00102818599752740);
// 	cv::Mat kernel1 = (Mat_<float>(11,1) <<  0.00102818599752740, 0.00759732401586496, 0.03599397767545871, 0.10934004978399577, 0.21296533701490150, 0.26596152026762182, 0.21296533701490150, 0.10934004978399577, 0.03599397767545871, 0.00759732401586496, 0.00102818599752740);

// 	Point anchor;
// 	anchor = Point( -1, -1 );
// 	double delta = 0;
	
// 	int x=img1_temp.rows, y=img1_temp.cols;
// 	//int nChan=img1_temp.channels()-2, d=CV_32FC1;
		
// 	cv::Mat img1_1 = cv::Mat( Size(x,y), CV_32FC1);
// 	cv::Mat img2_2 = cv::Mat( Size(x,y), CV_32FC1);
// 	cv::Mat img1_1_1 = cv::Mat( Size(x,y), CV_32FC1);
// 	cv::Mat img2_2_2 = cv::Mat( Size(x,y), CV_32FC1);
	
// 	img1_temp.convertTo(img1_1, CV_32FC1);
// 	img2_temp.convertTo(img2_2, CV_32FC1);
        
// 	// cvtColor(img1_1, img1_1_1, COLOR_BGR2GRAY, 1);
//     // cvtColor(img2_2, img2_2_2, COLOR_BGR2GRAY, 1);

// 	add( img1_1_1, Scalar(C3), img1_1_1 );
// 	add( img2_2_2, Scalar(C3), img2_2_2 );
	
// 	cv::Mat img1_sq = cv::Mat( Size(x,y), CV_32FC1);
// 	cv::Mat img2_sq = cv::Mat( Size(x,y), CV_32FC1);
// 	cv::Mat img1_img2 = cv::Mat( Size(x,y), CV_32FC1);

// 	cv::Mat img1_1_1_trans = cv::Mat( Size(y,x), CV_32FC1);
// 	cv::Mat img2_2_2_trans = cv::Mat( Size(y,x), CV_32FC1);

// 	transpose(img1_1_1, img1_1_1_trans);
// 	transpose(img2_2_2, img2_2_2_trans);
// 	arma::fmat imagine1_arma(img1_1_1_trans.ptr<float>(), img1_temp.rows, img1_temp.cols, true, false);
// 	arma::fmat imagine2_arma(img2_2_2_trans.ptr<float>(), img2_temp.rows, img2_temp.cols, true, false);
// 	arma::fmat imagine1_arma_floor = floor(imagine1_arma);
// 	arma::fmat imagine2_arma_floor = floor(imagine2_arma);

// 	cv::Mat img1_from_arma(y, x, CV_32FC1, imagine1_arma_floor.memptr());
// 	cv::Mat img1(img1_from_arma.t());
// 	cv::Mat img2_from_arma(y, x, CV_32FC1, imagine2_arma_floor.memptr());
// 	cv::Mat img2(img2_from_arma.t());
	
// 	pow( img1, 2, img1_sq);
// 	pow( img2, 2, img2_sq);
// 	multiply( img1, img2, img1_img2, 1, CV_32FC1 );

// 	cv::Mat mu1 = cv::Mat( Size(x,y), CV_32FC1);
// 	cv::Mat mu2 = cv::Mat( Size(x,y), CV_32FC1);

// 	cv::Mat mu1_sq( Size(x,y), CV_32FC1);
// 	cv::Mat mu2_sq( Size(x,y), CV_32FC1);
// 	cv::Mat mu1_mu2( Size(x,y), CV_32FC1);
	
// 	cv::Mat sigma1_sq = cv::Mat( Size(x,y), CV_32FC1);
// 	cv::Mat sigma2_sq = cv::Mat( Size(x,y), CV_32FC1);
// 	cv::Mat sigma1_sq_inter = cv::Mat( Size(x,y), CV_32FC1);
// 	cv::Mat sigma2_sq_inter = cv::Mat( Size(x,y), CV_32FC1);
// 	cv::Mat sigma12 = cv::Mat( Size(x,y), CV_32FC1);

// 	cv::Mat temp1 = cv::Mat( Size(x,y), CV_32FC1);
// 	cv::Mat temp2 = cv::Mat( Size(x,y), CV_32FC1);
// 	cv::Mat temp3 = cv::Mat( Size(x,y), CV_32FC1);

// 	cv::Mat mu1_inter = cv::Mat( Size(x,y), CV_32FC1);
// 	cv::Mat mu2_inter = cv::Mat( Size(x,y), CV_32FC1);
// 	cv::Mat sigma12_inter = cv::Mat( Size(x,y), CV_32FC1);
// 	cv::Mat ssim_map = cv::Mat( Size(x,y), CV_32FC1);
	
// 	/*************************** END INITS **********************************/


// 	//////////////////////////////////////////////////////////////////////////
// 	// PRELIMINARY COMPUTING

// 	filter2D(img1, mu1_inter, CV_32FC1, kernel1, anchor, delta, BORDER_REFLECT );
// 	filter2D(mu1_inter, mu1, CV_32FC1 , kernel, anchor, delta, BORDER_REFLECT );
	
// 	filter2D(img2, mu2_inter, CV_32FC1 , kernel1, anchor, delta, BORDER_REFLECT );	
// 	filter2D(mu2_inter, mu2, CV_32FC1 , kernel, anchor, delta, BORDER_REFLECT );
	
// 	pow( mu1, 2, mu1_sq );
// 	pow( mu2, 2, mu2_sq );
// 	multiply( mu1, mu2, mu1_mu2, 1, CV_32FC1 );
	
// 	filter2D(img1_sq, sigma1_sq_inter, CV_32FC1 , kernel1, anchor, delta, BORDER_REFLECT );
// 	filter2D(sigma1_sq_inter, sigma1_sq, CV_32FC1 , kernel, anchor, delta, BORDER_REFLECT );
// 	addWeighted( sigma1_sq, 1, mu1_sq, -1, 0, sigma1_sq, CV_32FC1 );

// 	filter2D(img2_sq, sigma2_sq_inter, CV_32FC1 , kernel1, anchor, delta, BORDER_REFLECT );	
// 	filter2D(sigma2_sq_inter, sigma2_sq, CV_32FC1, kernel, anchor, delta, BORDER_REFLECT );
// 	addWeighted( sigma2_sq, 1, mu2_sq, -1, 0, sigma2_sq, CV_32FC1 );
	
// 	filter2D(img1_img2, sigma12_inter, CV_32FC1 , kernel1, anchor, delta, BORDER_REFLECT );
// 	filter2D(sigma12_inter, sigma12, CV_32FC1 , kernel, anchor, delta, BORDER_REFLECT );
// 	addWeighted( sigma12, 1, mu1_mu2, -1, 0, sigma12, CV_32FC1 );
	

// 	//////////////////////////////////////////////////////////////////////////
// 	// FORMULA
	
// 	// (2*mu1_mu2 + C1)
// 	mu1_mu2.convertTo(temp1, CV_32FC1, 2);
// 	add( temp1, Scalar(C1), temp1, noArray(), CV_32FC1 );
	
// 	// (2*sigma12 + C2)
// 	sigma12.convertTo(temp2, CV_32FC1, 2);
// 	add( temp2, Scalar(C2), temp2, noArray(), CV_32FC1 );
	
// 	// ((2*mu1_mu2 + C1).*(2*sigma12 + C2))
// 	multiply( temp1, temp2, temp3, 1, CV_32FC1 );

// 	// (mu1_sq + mu2_sq + C1)
// 	add( mu1_sq, mu2_sq, temp1, noArray(), CV_32FC1 );
// 	add( temp1, Scalar(C1), temp1, noArray(), CV_32FC1 );
	
// 	// (sigma1_sq + sigma2_sq + C2)
// 	add( sigma1_sq, sigma2_sq, temp2, noArray(), CV_32FC1 );
// 	add( temp2, Scalar(C2), temp2, noArray(), CV_32FC1 );
	
// 	// ((mu1_sq + mu2_sq + C1).*(sigma1_sq + sigma2_sq + C2))
// 	multiply( temp1, temp2, temp1, 1, CV_32FC1 );
	
// 	// ((2*mu1_mu2 + C1).*(2*sigma12 + C2))./((mu1_sq + mu2_sq + C1).*(sigma1_sq + sigma2_sq + C2))
// 	divide( temp3, temp1, ssim_map, 1, CV_32FC1);
	
// 	Scalar index_scalar = mean( ssim_map, noArray() );
// 	return index_scalar;
// 	}


bool ends_with(const string &str, const string &suffix)
{
    return str.size() >= suffix.size() &&
           str.compare(str.size() - suffix.size(), suffix.size(), suffix) == 0;
}


vector<cv::Mat> get_patches(string left_img_path, int nw, int nh){
    vector<cv::Mat> patches;
    cv::Mat left_img = imread(left_img_path);
    cvtColor( left_img, left_img, COLOR_BGR2RGB );
    int width = left_img.size[0];
    int height = left_img.size[1];
    int channels = left_img.channels();
    int patch_h = (int)height/nh;
    int patch_w = (int)width/nw;

    for(int y = 0; y<nh; y++){
        for(int x = 0; x<nw; x++){
            int i = patch_w*x;
            int j = patch_h*y;
            
            int k = patch_w*(x+1);
            int l = patch_h*(y+1);
            
            patches.push_back(left_img(Rect(j, i, l-j, k-i)));
        }
    }

    return patches;
}


tuple<cv::Mat, vector<KeyPoint>> sift_feat(cv::Mat img_patch){
    
    cvtColor( img_patch, img_patch, COLOR_BGR2GRAY );
    Ptr<SIFT> siftPtr = SIFT::create();
    vector<KeyPoint> keypoints;
    siftPtr->detect(img_patch, keypoints);
    cv::Mat output;
    drawKeypoints(img_patch, keypoints, output);

    return make_tuple(output, keypoints);
}


int siftOnly_basedSwitching(string left_img, int n_patches = 4){
    n_patches = 4;
    vector<cv::Mat> patches = get_patches(left_img,n_patches,n_patches);

    // Computing sift applied patches
    vector<cv::Mat> sift_patches;
    vector<vector<KeyPoint>> sift_kps ;
    cv::Mat img;
    vector<KeyPoint> kp;
    for(int i=0; i < patches.size(); i++){
       tie(img , kp) = sift_feat(patches[i]);
       sift_patches.push_back(img);
       sift_kps .push_back(kp);
    }

    // Computing the sift based decision
    int tot_size = sift_patches[0].size[0]*sift_patches[0].size[1];
    vector<int> patch_binary;
    for(int i=0; i < sift_kps.size(); i++){
       if (sift_kps[i].size()<=(tot_size/1000)){
           patch_binary.push_back(0);
       }
       else{
           patch_binary.push_back(1);
       }
    }
    int winner = 3;
    int patch_binary_sum = accumulate(patch_binary.begin(), patch_binary.end(), 0);
    if ( patch_binary_sum > (n_patches*n_patches)/2){
        winner=2;
    }
    return winner;

}


int ssimNsift_basedSwitching(string left_img,
                              string lfnet_img,
                              string midas_img,
                              Ptr<StructuredEdgeDetection> pEdgeDetector){
    // reading left image                              
    cv::Mat image = imread(left_img);
    
    // BGR to RGB conversion
    cvtColor( image, image, COLOR_BGR2RGB );

    cv::Mat left_image_blurred;

    // blurring the image
    GaussianBlur(image,left_image_blurred, Size(5, 5),0);
    cv::Mat img_normalized;

    // Normalizing images
    left_image_blurred.convertTo(img_normalized, CV_32FC3, 1.f/255.0, 0); 

    // reading midas image
    cv::Mat midas_image = imread(midas_img);
    cv::Mat processed_midas_image;
    cv::Mat mul_midas_image;
    // calculating min and max of midas image
    double min, max;
    minMaxLoc(midas_image, &min, &max);

    // Check for division by 0 before min max norm.
    if (max-min!=0){
        cv::Mat midas_image_1 = midas_image - Scalar(min, min, min);
        Scalar factor = Scalar(1, 1, 1) / (Scalar(max, max, max) - Scalar(min, min, min));
        mul_midas_image = midas_image_1.mul(factor);
        mul_midas_image.convertTo(processed_midas_image, CV_32FC3); 
        minMaxLoc(processed_midas_image, &min, &max);
    }
    else{
        processed_midas_image = cv::Mat::zeros(midas_image.size(), CV_32FC3);
    }
    // variable for if NAN in LFNet image.
    bool nan_goSF=false;
    cv::Mat lfnet_image;

    //Reading the lfnet image (.pfm, .jpg, .png is supported).
    if (ends_with(lfnet_img, ".pfm")){

        // reading image
        lfnet_image = imread(lfnet_img, IMREAD_UNCHANGED);

        //if no NAN found in lfnet_img
        if (checkRange(lfnet_image)){
            // check for division by 0. (Because in image all disparity are same.)
            minMaxLoc(lfnet_image, &min, &max);
            if (max-min!=0){
                cv::Mat lfnet_image_1 = lfnet_image - Scalar(min, min, min);
                Scalar factor = Scalar(1, 1, 1) / (Scalar(max, max, max) - Scalar(min, min, min));
                lfnet_image = lfnet_image_1.mul(factor);
            }
            else{
                nan_goSF = true;
                lfnet_image = cv::Mat::zeros(lfnet_image.size(), CV_32FC3);
            }
        }
    }
    else{
        lfnet_image = imread(lfnet_img);
        lfnet_image.convertTo(lfnet_image, CV_32F, 1.f/255.0, 0); 
    }


    // cout << "successfully done unmtil now";

    // if lfnet_img is single channeled greyscale img then make it 3 channeled greyscale image. 
    if(lfnet_image.channels()==1){
        cvtColor( lfnet_image, lfnet_image, COLOR_GRAY2BGR );
    }

    // Performing the edge detection.
    cv::Mat edges_left_raw(img_normalized.size(), img_normalized.type());
    pEdgeDetector->detectEdges(img_normalized, edges_left_raw);
    
    cv::Mat edges_midas_raw(processed_midas_image.size(), processed_midas_image.type());
    pEdgeDetector->detectEdges(processed_midas_image, edges_midas_raw);

    cv::Mat edges_lfnet(lfnet_image.size(), lfnet_image.type());
    pEdgeDetector->detectEdges(lfnet_image, edges_lfnet);

    // Min Max Norm the edge map between (0,255) with check for division by 0.
    // Left_edges
    cv::Mat edges_left;
    cv::Mat edges_midas;
    minMaxLoc(edges_left_raw, &min, &max);
    if (max-min!=0){
        cv::Mat edges_left_1 = edges_left_raw - Scalar(min, min, min);
        cv::Mat edges_left_2 = Scalar(255, 255, 255) / (Scalar(max, max, max) - edges_left_raw);
        edges_left = edges_left_1.mul(edges_left_2);
    }
    else{
        edges_left = cv::Mat::zeros(edges_left_raw.size(), CV_32FC1);
    }

    Mat mask = (edges_left != edges_left);
    int numNaNs = countNonZero(mask);
    patchNaNs(edges_left, 0);

    // midas_edges
    minMaxLoc(edges_midas_raw, &min, &max);
    if (max-min!=0){
        cv::Mat edges_midas_1 = edges_midas_raw - Scalar(min, min, min);
        cv::Mat edges_midas_2 = Scalar(255, 255, 255) / (Scalar(max, max, max) - edges_midas_raw);
        edges_midas = edges_midas_1.mul(edges_midas_2);
    }
    else{
        edges_midas = cv::Mat::zeros(edges_midas_raw.size(), CV_32FC1);
    }

    mask = (edges_midas != edges_midas);
    numNaNs = countNonZero(mask);
    patchNaNs(edges_midas, 0);

    // Lfnet_edges
    minMaxLoc(edges_lfnet, &min, &max);
    if (max-min!=0){
        cv::Mat edges_lfnet_1 = edges_lfnet - Scalar(min, min, min);
        cv::Mat edges_lfnet_2 = Scalar(255, 255, 255) / (Scalar(max, max, max) - edges_lfnet);
        edges_lfnet = edges_lfnet_1.mul(edges_lfnet_2);
    }
    else{
        edges_lfnet = cv::Mat::zeros(edges_lfnet.size(), CV_32FC1);
    }

    mask = (edges_lfnet != edges_lfnet);
    numNaNs = countNonZero(mask);
    patchNaNs(edges_lfnet, 0);

    // Calculating SSIM between (SF_edges and left_edges) and (LF_edge and SF_edges).
    // Giving block_size = 7 because in skimage official impl it is 7
    // https://github.com/scikit-image/scikit-image/blob/main/skimage/metrics/_structural_similarity.py#L164
    // double ssim_number = ssim(edges_left,edges_midas, 7, true);
    // cout << "SSIm " << ssim_number << endl;

    Scalar ssim__midas = getMSSIM(edges_left,edges_midas);
    Scalar ssim__lfnet = getMSSIM(edges_lfnet,edges_left);
    double ssim_midas = ssim__midas.val[0];
    double ssim_lfnet = ssim__lfnet.val[0];
    cout << "ssim_midas from myssim" << ssim_midas << endl;
    cout << "ssim_lfnet from myssim" << ssim_lfnet << endl;
    // cout << " MSSIM: "
    //             << " R " << setiosflags(ios::fixed) << setprecision(2) << ssim_lfnet.val[2] * 100 << "%"
    //             << " G " << setiosflags(ios::fixed) << setprecision(2) << ssim_lfnet.val[1] * 100 << "%"
    //             << " B " << setiosflags(ios::fixed) << setprecision(2) << ssim_lfnet.val[0] * 100 << "%";
        
    //  meaning_of_my_decision
    //     2="LFNet is better."
    //     3="SFNet is better."

    // edge ssim based decision.
    int my_decision=0;
    if (ssim_midas>ssim_lfnet){
        //  if midas better via ssim go midas.
        my_decision=3;
    }
    else{
        int sift_winner = siftOnly_basedSwitching(left_img, 4);
        //  now make decision based on sift based prediction.
        if(sift_winner==2){
            my_decision=2;
        }
        else{
            my_decision=3;
        }

    }

    // // If nan directly go SF.
    if (nan_goSF == true)
        my_decision=3;
    cout << "my_decision" << my_decision << endl;
    return my_decision;
}


int main(){
    string model_path = "model/model.yml";
    // string IMG_FOLDER = "/home/yogesh/Documents/median-df/two_class_cpp/";
    string CENTER_FOLDER = "/home/yogesh/Documents/median-df/two_class_cpp/data_45/21_05/";
    string LFNET_FOLDER = "/home/yogesh/Documents/median-df/two_class_cpp/data_45/21_05_lfnet/";
    string SFNET_FOLDER = "/home/yogesh/Documents/median-df/two_class_cpp/data_45/21_05_sfnet/";
    // int n = 15; // number of samples to get output from
    // for(int i=0;i<n;i++){
    //     // if(i==7 || i==8){
    //     //     continue;
    //     // }
    //     string center = string(CENTER_FOLDER) + to_string(i+1) +string(".jpg");
    //     string lfnet = string(LFNET_FOLDER) + to_string(i+1) +string("/LFNET_OUTPUT/lfnet_disparity.pfm");
    //     string sfnet = string(SFNET_FOLDER) + to_string(i+1) +string("/test.tiff_4.png");
    //     // get_patches(img_path, 3, 3);
    //     cv::Ptr<StructuredEdgeDetection> pEdgeDetector = cv::ximgproc::createStructuredEdgeDetection(model_path);
    //     cout << "Output for image pair " << i+1  <<  center << ":: " ;
    //     cout  << ssimNsift_basedSwitching(center, lfnet, sfnet, pEdgeDetector) << endl;
    // }

    for (const auto & entry : fs::directory_iterator(LFNET_FOLDER)){
        string path = string(entry.path());
        path = path.substr(path.find("lfnet/") + 6); 
        string center = CENTER_FOLDER + path +string("/h_4.jpg");
        // string center = CENTER_FOLDER + path  +string("/rectview__2.tiff");
        string lfnet = LFNET_FOLDER + path  +string("/Center_disp_0.pfm");
        // string sfnet = SFNET_FOLDER + path  +string("/rectview__2.png");
        string sfnet = SFNET_FOLDER + path  +string("/h_4.png");
        cout << "center " << center << endl;
        cout << "lfnet " << lfnet << endl;
        cout << "sfnet " << sfnet << endl;

        // get_patches(img_path, 3, 3);
        cv::Ptr<StructuredEdgeDetection> pEdgeDetector = cv::ximgproc::createStructuredEdgeDetection(model_path);
        cout  << ssimNsift_basedSwitching(center, lfnet, sfnet, pEdgeDetector) << endl;
    }
return 0;
}